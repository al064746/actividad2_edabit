
package actividad2_estructura_de_datos;

public class ACTIVIDAD2_PROGRAMA4 {

    public static void main(String[] args) {
      public class Challenge {
	public static boolean canCapture(String[] rooks) {
		return ((rooks[0].charAt(0) == rooks[1].charAt(0)) || 
						(rooks[0].charAt(1) == rooks[1].charAt(1)));
	}
}
    }
    
}
